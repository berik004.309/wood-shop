from datetime import datetime
from shop import db, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer,unique=True, primary_key=True,default=0)
    name = db.Column(db.String(50),nullable=False)
    price = db.Column(db.Integer,nullable=False)
    img_src = db.Column(db.String,default="")

class Customer(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(255))
    password = db.Column(db.String(60),index=True,default=0)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
        print(self.password_hash)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

@login_manager.user_loader
def load_user(user_id):
    return Customer.query.get(int(user_id))

class Basket(db.Model):
    id = db.Column(db.Integer, primary_key=True,unique=True,autoincrement=True)
    custid = db.Column(db.Integer, db.ForeignKey('customer.id'))
    itemid = db.Column(db.Integer, db.ForeignKey('item.id'))
    volume = db.Column(db.Integer,default=0)
    
class Review(db.Model):
    __tablename__ = 'review'
    id = db.Column(db.Integer, primary_key=True,unique=True,autoincrement=True)
    itemid = db.Column(db.Integer, db.ForeignKey('item.id'))
    rev_text = db.Column(db.String(500),nullable=False)
