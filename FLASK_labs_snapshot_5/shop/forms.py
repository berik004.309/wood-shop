from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, Regexp
from shop.models import Customer


class RegistrationForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired('Please enter your username'), Length(min=3, max=15)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    
    password = PasswordField('Password', validators=[DataRequired(),Regexp('^.{6,16}$',
                              message='Your password should be between 6 and 16 characters long.')])
    
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password', message='Passwords must match')])
    submit = SubmitField('Register')

    def validate_username(self, name):
        user = Customer.query.filter_by(username=name.data).first()
        if user:
            raise ValidationError('Username already exist. Please choose a different one.')

    def validate_email(self, email):
        user = Customer.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('This email is already registered. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

class ReviewForm(FlaskForm):
    rev_text = StringField('Please leave a review: ')
    submit = SubmitField('Submit Review')

class PaymentForm(FlaskForm):
    name_on_card = StringField("Name on Card ", validators=[DataRequired("Please enter ")])
    card_number = StringField("Card Num ", validators=[DataRequired("Please enter "), Regexp('^4[0-9]{12}(?:[0-9]{3})?$')])
    expiry_date = StringField("Exp Date ", validators=[DataRequired("Please enter "), Regexp('^\d{2}\/\d{2}$')])
    cvs = StringField("CVS", validators=[DataRequired("Please enter"), Length(min=3,max=3)])
    submit = SubmitField('Checkout')

